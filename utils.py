import subprocess
import sys
import re

v4l = subprocess.run(['v4l2-ctl', '--list-devices'], stdout=subprocess.PIPE)
v4l = v4l.stdout.decode("utf-8")

if not ("Dummy" in v4l):
    sys.exit("No dummy video found. Please run 'ml configure webcam'.")

v4li = [int(i) for i in re.findall(r"/dev/video(.)", v4l)]

# ----------------------------------------------------------------------
# Camera devices.
#
# Listed with v4l2-ctl --list-devices
#
# If Dummy is available then that would be the first video device from
# the above command. Assum the first of the non-dummy come next, and
# the first is the default webcam. Must be better ways to determine
# this.
#
# ----------------------------------------------------------------------

WEBCAM = v4li[1]  # Usually /dev/video0 or /dev/video1.
DUMCAM = v4li[0]  # Created with sudo modprobe v4l2loopback devices=1
DUMDEV = f'/dev/video{DUMCAM}'

# ----------------------------------------------------------------------
# Width and height of output screen.
# ----------------------------------------------------------------------

OUT_W = 1280
OUT_H = 720
