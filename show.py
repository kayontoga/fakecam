import argparse
import pyfakewebcam
import numpy as np
import cv2
import time
import os
import sys
import timeit

from mlhub.pkg import get_cmd_cwd
from PIL import Image

from utils import DUMDEV, OUT_W, OUT_H

# Replace the webcam stream with a static image.

# ----------------------------------------------------------------------
# Parse command line arguments
# ----------------------------------------------------------------------

option_parser = argparse.ArgumentParser(add_help=False)

option_parser.add_argument(
    'path',
    help='path to image file')

args = option_parser.parse_args()
path = os.path.join(get_cmd_cwd(), args.path)

if not os.path.isfile(path):
    sys.exit(f'mlhub webcam show: file not found "{path}"')

# ----------------------------------------------------------------------
# Crteate a background image of the right size for the video stream.
# ----------------------------------------------------------------------

bg = Image.new("RGBA", (OUT_W, OUT_H), "white")

# ----------------------------------------------------------------------
# Open the image to be displayed on the dummy camera, resized to fit.
# ----------------------------------------------------------------------

img = Image.open(path)
img.thumbnail((OUT_W-40, OUT_H-80), Image.ANTIALIAS)
img = img.convert("RGBA")

# ----------------------------------------------------------------------
# Paste the loaded image on to the background.
# ----------------------------------------------------------------------

w,h = img.size

bg.paste(img, (round((OUT_W/2)-(w/2)), round((OUT_H/2)-(h/2))), img)
img = bg.convert("RGB")

# ----------------------------------------------------------------------
# Output to the dummy camera.
# ----------------------------------------------------------------------

try:
    camera = pyfakewebcam.FakeWebcam(DUMDEV, OUT_W, OUT_H)
except FileNotFoundError:
    sys.exit()

img = np.array(img)

try:
    while True:
        camera.schedule_frame(img)
        time.sleep(1/30)
except KeyboardInterrupt:
    sys.exit(0)
