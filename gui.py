#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A MLHub GUI for WebCam Privacy

author: Graham Williams
website: https://gitlab.com/kayontoga/webcam
"""

import os
import wx
import subprocess
import signal

from mlhub.pkg import get_cmd_cwd

MODEL = "MLHub WebCam Privacy"
CMD_SETUP = ["ml", "show", "webcam", "~/.mlhub/webcam/notrans.jpg"]
CMD_WEBCAM = ["ml", "thru", "webcam"]
CMD_LOGO = ["ml", "logo", "webcam"]
CMD_LOOP = ["ml", "loop", "webcam"]
CMD_BLUR = ["ml", "blur", "webcam"]
CMD_EDGE = ["ml", "edge", "webcam"]
CMD_EMBOSS = ["ml", "emboss", "webcam"]
CMD_CONTOUR = ["ml", "contour", "webcam"]

# TODO Allow movie or static image and "LOOP/SHOW" as appropriate.

DEFAULT_PATH = "Logo (default=logo.png) or Loop (default=loop.mp4)"
DEFAULT_LOOP = os.path.join(get_cmd_cwd(), "loop.mp4")
DEFAULT_LOGO = os.path.join(get_cmd_cwd(), "logo.png")

WILDCARD = "Logo (*.png;*.jgp)|*.png;*.jpg|" \
           "Loop (*.mp4,*.avi)|*.mp4;*.avi|" \
           "All files (*.*)|*.*"


def killbill():
    """Kill the running python3 process on /dev/video4 assuming it's ours!"""
    cmd = "lsof /dev/video4 | grep python3 | head -1 | awk '{print $2}'"
    ps = subprocess.run(cmd, shell=True,
                        stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    pid = ps.stdout.decode("utf-8").rstrip()
    if len(pid):
        os.kill(int(pid), signal.SIGINT)


class MLHub(wx.Frame):

    def __init__(self, parent, title):
        super(MLHub, self).__init__(parent,
                                    title=title,
                                    size=(650, 200))

        self.InitUI()

    def InitUI(self):
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        self.movies_dir = os.path.join(os.getcwd(), "cache/movies")
        self.last_browse_dir = ""
        self.subprocess = None

        self.SetIcon(wx.Icon("logo-mlhub.svg"))

        # TODO Check for Dummy video device as output by v4l2-ctl
        # --list-devices. If not found then prompt user to run the
        # command sudo modprobe v4l2loopback devices=1

        panel = wx.Panel(self)

        vbox = wx.BoxSizer(wx.VERTICAL)

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.tc_path = wx.TextCtrl(panel, value=DEFAULT_PATH)
        hbox1.Add(self.tc_path, proportion=1)
        bt_browse = wx.Button(panel, label="Browse")
        bt_browse.Bind(wx.EVT_BUTTON, self.OnBrowse)
        hbox1.Add(bt_browse, flag=wx.LEFT, border=10)
        vbox.Add(hbox1,
                 flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP,
                 border=10)

        vbox.Add((-1, 10))

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        # SETUP
        bt_setup = wx.Button(panel, label="Setup")
        bt_setup.Bind(wx.EVT_BUTTON, self.OnSETUP)
        bt_setup.SetFocus()
        hbox2.Add(bt_setup, flag=wx.RIGHT, border=10)
        # WEBCAM
        bt_webcam = wx.Button(panel, label="WebCam")
        bt_webcam.Bind(wx.EVT_BUTTON, self.OnWEBCAM)
        hbox2.Add(bt_webcam, flag=wx.RIGHT, border=10)
        # LOGO
        bt_logo = wx.Button(panel, label="Logo")
        bt_logo.Bind(wx.EVT_BUTTON, self.OnLOGO)
        hbox2.Add(bt_logo, flag=wx.RIGHT, border=10)
        # LOOP
        bt_loop = wx.Button(panel, label="Loop")
        bt_loop.Bind(wx.EVT_BUTTON, self.OnLOOP)
        hbox2.Add(bt_loop, flag=wx.RIGHT, border=10)
        # FREEZE
        bt_freeze = wx.Button(panel, label="Freeze")
        bt_freeze.Bind(wx.EVT_BUTTON, self.OnFREEZE)
        hbox2.Add(bt_freeze, flag=wx.RIGHT, border=10)
        # Add to the panel.
        vbox.Add(hbox2,
                 flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP,
                 border=10)

        vbox.Add((-1, 10))

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        # BLUR
        bt_blur = wx.Button(panel, label="Blur")
        bt_blur.Bind(wx.EVT_BUTTON, self.OnBLUR)
        hbox3.Add(bt_blur, flag=wx.RIGHT, border=10)
        # EDGE
        bt_edge = wx.Button(panel, label="Edge")
        bt_edge.Bind(wx.EVT_BUTTON, self.OnEDGE)
        hbox3.Add(bt_edge, flag=wx.RIGHT, border=10)
        # EMBOSS
        bt_emboss = wx.Button(panel, label="Emboss")
        bt_emboss.Bind(wx.EVT_BUTTON, self.OnEMBOSS)
        hbox3.Add(bt_emboss, flag=wx.RIGHT, border=10)
        # CONTOUR
        bt_contour = wx.Button(panel, label="Contour")
        bt_contour.Bind(wx.EVT_BUTTON, self.OnCONTOUR)
        hbox3.Add(bt_contour, flag=wx.RIGHT, border=10)
        # HELP
        bt_help = wx.Button(panel, label="Help")
        bt_help.Bind(wx.EVT_BUTTON, self.OnHELP)
        hbox3.Add(bt_help, flag=wx.RIGHT, border=10)
        # # LISTENING
        # bt_listening = wx.Button(panel, label="Listening")
        # bt_listening.Bind(wx.EVT_BUTTON, self.OnLISTENING)
        # hbox2.Add(bt_listening, flag=wx.RIGHT, border=10)
        # Add to the panel.
        vbox.Add(hbox3,
                 flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP,
                 border=10)

        vbox.Add((-1, 10))

        panel.SetSizer(vbox)

    def OnBrowse(self, event):
        if self.last_browse_dir == "":
            default_dir = self.movies_dir
        else:
            default_dir = self.last_browse_dir
        dlg = wx.FileDialog(self,
                            message="Choose a file",
                            defaultDir=default_dir,
                            defaultFile="",
                            wildcard=WILDCARD,
                            style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR
                            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            if len(paths):
                self.last_browse_dir = os.path.dirname(paths[0])
                # Display path in text control.
                self.tc_path.SetValue(paths[0])

    def OnSETUP(self, event):
        cmd = CMD_SETUP.copy()
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnWEBCAM(self, event):
        cmd = CMD_WEBCAM.copy()
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnLOGO(self, event):
        path = self.tc_path.GetValue()
        if path == DEFAULT_PATH:
            path = DEFAULT_LOGO
        cmd = CMD_LOGO.copy()
        cmd.append(path)
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnLOOP(self, event):
        path = self.tc_path.GetValue()
        if path == DEFAULT_PATH:
            path = DEFAULT_LOOP
        cmd = CMD_LOOP.copy()
        cmd.append(path)
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnFREEZE(self, event):
        if self.subprocess:
            killbill()

    def OnBLUR(self, event):
        cmd = CMD_BLUR.copy()
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnEDGE(self, event):
        cmd = CMD_EDGE.copy()
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnEMBOSS(self, event):
        cmd = CMD_EMBOSS.copy()
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnCONTOUR(self, event):
        cmd = CMD_CONTOUR.copy()
        # Show the command line.
        print("$ " + " ".join(cmd))
        if self.subprocess:
            killbill()
        self.subprocess = subprocess.Popen(cmd)

    def OnHELP(self, event):
        print("")
        print("To start a dummy video device: sudo modprobe v4l2loopback "
              "devices=1")
        print("To list available video devices: v4l2-ctl --list-devices")
        print("To remove the dummy video device: sudo rmmod v4l2loopback")
        print("")
        
    def OnClose(self, event):
        killbill()
        self.Destroy()


def main():
    app = wx.App()
    mlhub = MLHub(None, title='MLHub: ' + MODEL)
    mlhub.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
